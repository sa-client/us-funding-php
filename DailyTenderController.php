<?php
/**
 * Cli DailyTender controller
 * 
 * @category Cli
 * @package Cli\Controller
 */

namespace Cli\Controller;

use Web\Cli\CliController;
use Cli\Model\DailyTenderModel;


class DailyTenderController extends CliController
{
    private $_model;
        
    public function __construct()
    {
        parent::__construct();
        $this->_model = new DailyTenderModel();
    }
    
    public function indexAction()
    {
        $this->start('Daily Tender Controller');
        $date = '2017-10-18';
        $this->out($date);
        $sendTender = $this->_model->getSendTenders($date);
        if (! empty($sendTender)) {
            if ($sendTender->processed > 0) {
                $tenders = unserialize($sendTender->tenders);
                $tenederStatus = $this->_model->getTenderStatus($tenders);
                $sendLog = $this->_model->getSendTenderLog($date);
                if (! empty($sendLog)) {
                    $this->_model->loadPreparedStatements();
                    try {
                        $this->_model->getAdapter()->beginTransaction();
                        foreach ($sendLog as $log) {
                            echo ' , ' . $log->subscriberId;
                            $tenderIds = unserialize($log->tenders);
                            $loggedTenders = $this->_model->getSubscriberTenders($log->subscriberId, $tenderIds);
                            if (empty($loggedTenders)) {
                                $this->_model->insertSubscriberTenders($log->subscriberId, $tenderIds);
                                echo ' full ';
                            } elseif (count($loggedTenders) < count($tenderIds)) {
                                echo ' new ';
                                $newTenderIds = [];
                                foreach ($tenderIds as $newId) {
                                    if (! in_array($newId, $loggedTenders)) {
                                        if (in_array($newId, $tenederStatus)) {
                                            $newTenderIds[] = $newId;
                                        }
                                    }
                                }
                                if (! empty($newTenderIds)) {
                                    $this->_model->insertSubscriberTenders($log->subscriberId, $newTenderIds);
                                }
                            } else {
                                echo ' ok ';
                            }
                        }
                        $this->_model->getAdapter()->commit();
                    } catch (\PDOException $e) {
                        $this->_model->getAdapter()->rollBack();
                        echo $e->getMessage();
                    }
                    
                }
            }
        }
        
        $this->now();

        $this->finish();
    }
    
}
