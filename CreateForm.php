<?php
/**
 * Backend Tender Create Form
 * 
 * @category Backend
 * @package  Backend\Form
 * @subpackage Backend\Form\Tender
 */

namespace Backend\Form\Tender;

use Web\Form\Form;
use Web\Utility\DateHelper;
use Web\Form\Document;
use Web\Form\UploadDirectory;
use Backend\Constant\FileTypeConstant;
use Backend\Constant\TenderConstant;


class CreateForm extends Form
{
    private $_documents = [];
    private $_documentLimit = 3;
    
    public function __construct()
    {
        parent::__construct();
        $fields = [
            'clientId', 
            'tenderDate',
            'deadline',
            'sourceId',
            'tenderTypeId',
            'city',
            'countryId',
            'international',
            'premium',
            'postedBy',
            'language',
            'surfaceArea',
            'constructionValue',
            'serviceFee',
            'contactName',
            'telephone',
            'email', 
            'fax', 
            'url',
            'clickHere',
            'showClickHere',
            'mainSectorId',
            'emailDescription',
            'description',
            'allsectors',
            'sectors',
            'references',
        ];
        $checkboxes = [ 'international', 'premium', 'showClickHere', 'allsectors' ];
        $multiselect = [ 'sectors', 'references' ];
        $this->addFields($fields);
        $this->checkboxes($checkboxes);
        $this->multiselect($multiselect);
    }
    
    /**
     * set document limit
     * 
     * @param int $limit
     */
    public function setDocumentLimit($limit)
    {
        $this->_documentLimit = $limit;
    }
    
    /**
     * validate form
     *
     * @return boolean
     */
    public function isValid()
    {
        $this->csrf();
        $this->postback();
        $values = $this->getValues();
        if ($this->validator->isNull($values['clientId'])) {
            $this->error->required('Client ID');
        }
        if ($this->validator->isNull($values['tenderDate'])) {
            $this->error->required('Tender Date');
        }
        if ($this->validator->isNull($values['deadline'])) {
            $this->error->required('Deadline');
        }
        if ($this->validator->isNull($values['city'])) {
            $this->error->required('City');
        }
        if (! $this->validator->selected($values['countryId'])) {
            $this->error->notSelected('Country');
        }

        if (! $this->validator->selected($values['mainSectorId'])) {
            $this->error->notSelected('Main sector');
        }
        if ($this->validator->isNull($values['emailDescription'])) {
            $this->error->required('Email description');
        }
        if ($this->validator->isNull($values['description'])) {
            $this->error->required('Description');
        }
        
        if ($this->noError()) {
            for ($count = 1; $count <= $this->_documentLimit; $count++) {
                $document = new Document('document' . $count, 'Document ' . $count);
                if (! $document->isValid()) {
                    $this->addError($document->getError());
                }
                $this->_documents[$count] = $document;
            }
        }
        
        return $this->noError();
    }
    
    /**
     * create record
     * 
     * @return int
     */
    public function create()
    {
        $now = DateHelper::now();
        $values = $this->getValues();
        $tender = [
            'clientId' => $values['clientId'],
            'tenderDate' => DateHelper::fromDatepicker($values['tenderDate']),
            'deadline' => DateHelper::fromDatepicker($values['deadline']),
            'sourceId' => (int) $values['sourceId'],
            'tenderTypeId' => (int) $values['tenderTypeId'],
            'city' => $values['city'],
            'countryId' => (int) $values['countryId'],
            'international' => (int) $values['international'],
            'postedBy' => $values['postedBy'],
            'language' => $values['language'],
            'surfaceArea' => $values['surfaceArea'],
            'constructionValue' => $values['constructionValue'],
            'serviceFee' => $values['serviceFee'],
            'contactName' => $values['contactName'],
            'telephone' => $values['telephone'],
            'email' => $values['email'],
            'fax' => $values['fax'],
            'url' => $values['url'],
            'showClickHere' => (int) $values['showClickHere'],
            'mainSectorId' => (int) $values['mainSectorId'],
            'emailDescription' => $values['emailDescription'],
            'description' => $values['description'],
            'textDescription' => strip_tags($values['description']),
            'created' => $now,
            'updated' => $now,
            'premium' => $values['premium'],
        ];

        if ('' != $values['clickHere']) {
            $tender['clickHere'] = $this->_fixUrl($values['clickHere']);
        }
        $tenderCode = $this->_getTenderCode($values['emailDescription']);
        while ($this->getModel()->checkTenderCode($tenderCode)) {
            $tenderCode = $this->_getTenderCode($values['emailDescription']);
        }
        $tender['tenderCode'] = $tenderCode;
      
      
  $id = $this->save($tender);
        $sectors = $values['sectors'];
        if (! in_array($values['mainSectorId'], $sectors)) {
            $sectors[] = $values['mainSectorId'];
        }
        $this->getModel()->addTenderSectors($id, $sectors);
	$this->_uploadDocuments($id);

       return $id;
    }
    
    
    /**
     * get tender code
     * 
     * @param stirng $salt
     * @return string
     */
    private function _getTenderCode($salt)
    {
        $random = base64_encode(rand(0, 10000) . md5($salt) .  time());
        $cleaned = preg_replace('/[^a-z0-9]+/i', '', $random);
        $start = rand(0, strlen($cleaned) - 9);
        return strtoupper(substr($cleaned, $start, 8));
    }
    
    /**
     * upload document
     * 
     * @param int $tenderId
     */
    private function _uploadDocuments($tenderId)
    {
        $fileTypes = [
            'pdf' => FileTypeConstant::PDF,
            'doc' => FileTypeConstant::DOC,
            'docx' => FileTypeConstant::DOCX,
            'zip' => FileTypeConstant::ZIP,
        ];

        $directoryHandler = new UploadDirectory();
	 $path = $directoryHandler->run(UploadDirectory::TENDER);
	$documentPath = TenderConstant::UPLOAD_PATH . DS . $path;
        $references = $this->getValue('references');
        $default = [
            'tenderId' => $tenderId,
            'title' => '',
            'reference' => '',
            'filename' => '',
            'path' => $path,
            'fileType' => 0,
        ];
        for ($count = 1; $count <= $this->_documentLimit; $count++) {
            if (isset($this->_documents[$count])) {
                $documentUpload = $this->_documents[$count];
                if ($documentUpload->hasFile()) {
                    $documentUpload->setUploadPath(UPLOAD_DIR);
                    if ($documentUpload->upload($documentPath)) {
                        $document = $default;
                        $document['title'] = $documentUpload->getRawFilename();
                        $document['filename'] = $documentUpload->getUploadedFilename();
                        if (isset($references[$count])) {
                            $document['reference'] = $references[$count];
                        }
                        $extension = $documentUpload->getUploadedFileExtension();
                        if (isset($fileTypes[$extension])) {
                            $document['fileType'] = $fileTypes[$extension];
                        }
                        $this->getModel()->addDocument($document);
                    }
                }
            }
        }
    }
    
    /**
     * fix url protocol
     * 
     * @param string $url
     * @return string
     */
    private function _fixUrl($url)
    {
        if (0 == preg_match('#^http(s)?://#i', $url)) {
            return 'http://' . $url;
        }
        return $url;
    }

}
