<?php
/**
 * Application Winning News Model
 * 
 * @category Application
 * @package  Application\Model
 */

namespace Application\Model;

use Application\Constant\WinningNewsConstant;
use Web\Database\TableGateway;
use Web\Database\Table\TEDWeb;


class WinningNewsModel extends TableGateway
{
    
    public function __construct()
    {
        parent::__construct(TEDWeb::WINNING_NEWS);
    }
    
    /**
     * get active article count in year
     * 
     * @param int $year
     * @return int
     */
    public function activeArticleTotal($year)
    {
        $where = $this->_bindActive() . ' AND ' 
               . $this->getHelper()->bindId($year, 'articleYear');
        return $this->selectTotal($where);
    }
    
    /**
     * find latest year
     * 
     * @return int
     */
    public function getLatestActiveYear()
    {
        return $this->selectMax('articleYear', $this->_bindActive());
    }
    
    /**
     * get active articles in year
     * 
     * @param int $year
     * @return array
     */
    public function getArticles($year)
    {
        $where = $this->_bindActive() . ' AND ' 
               . $this->getHelper()->bindId($year, 'articleYear');
        return $this->select($where, 'id DESC');
    }

    /**
     * get active articles by search
     *
     * @param int $year
     * @return array
     */
    public function getArticlesBySearch($year,$keyword)
    {
        $where = $this->_bindActive() . ' AND '
            . $this->getHelper()->bindId($year, 'articleYear');
        $table = new TableGateway(TEDWeb::COUNTRY);
        $countries = $table->selectPairs(['id','country']);
        $countries = array_map('strtolower', $countries);
        $input = preg_quote(strtolower($keyword), '~'); // don't forget to quote input string!
        $result = preg_filter('~' . $input . '~','$0', $countries);
        $where .= ' AND (`title` LIKE \'%'.$keyword.'%\' OR `subtitle` LIKE \'%'.$keyword.'%\' OR `city` LIKE \'%'.$keyword.'%\' OR `description` LIKE \'%'.$keyword.'%\' OR `architect` LIKE \'%'.$keyword.'%\' OR `website` LIKE \'%'.$keyword.'%\' ';
        if(count($result) > 0) {
            $where .= ' OR countryId IN(';
            foreach ($result as $key => $value) {
                $where .=  $key.',';
            }
            $where = rtrim($where,',');
            $where .= ')';
        }
        $where .= ' )';
        return $this->select($where, 'id DESC');
    }
    
    /**
     * check if article is active
     * 
     * @param int $id
     * @return boolean
     */
    public function isActiveArticle($id)
    {
        $where = $this->_bindActive() . ' AND ' . $this->getHelper()->bindId($id);
        return $this->selectTotal($where) > 0;
    }
    
    /**
     * get years with active articles
     * 
     * @return array
     */
    public function getYears()
    {
        $query = 'SELECT DISTINCT `articleYear` FROM `' . $this->getTable() 
              . '` WHERE `active` = 1 ORDER BY `articleYear` DESC';
        return $this->queryColumns($query);
    }
    
    /**
     * get article navigation slug
     * 
     * @param array $where
     * @param string $order
     * @return string
     */
    public function getArticleNavigationSlug($where, $order)
    {
        if ($this->selectTotal($where->getWhere()) > 0) {
            return $this->selectColumn('slug', $where->getWhere(), $order);
        }
        return '';
    }
    
    /**
     * check if given year exists In the Winning_News_Year table 
     * 
     * @param int $year
     * @return int
     */
    public function articleYearExists($year)
    {
        return $this->getTableGateway(TEDWeb::WINNING_NEWS_YEAR)->selectTotalByColumn('articleYear', $year, true);
    }
    
    /**
     * get country options
     * 
     * @return array
     */
    public function getCountryOptions()
    {
        return $this->getTableGateway(TEDWeb::COUNTRY)->selectPairs(['id', 'country']);
    }
    
    /**
     * get country by id
     * 
     * @param int $countryId
     * @return string
     */
    public function getCountryById($countryId)
    {
        return $this->getTableGateway(TEDWeb::COUNTRY)->selectColumnById('country', $countryId);
    }
    
    /**
     * get article images
     * 
     * @param int $articleId
     * @return array
     */
    public function getArticleImages($articleId)
    {
        $table = $this->getTableGateway(TEDWeb::WINNING_NEWS_IMAGE);  
        return $table->select($table->getHelper()->bindId($articleId, 'winningNewsId'));
    }
    
    /**
     * check article year
     * 
     * @param int $year
     * @return boolean
     */
    public function checkArticleYear($year)
    {
        $where = $this->getHelper()->bindId($year, 'articleYear')
               . ' AND ' .  $this->_bindActive();
        return $this->selectTotal($where) > 0;
    }
    
    /**
     * get default year
     *  
     * @return int
     */
    public function getDefaultYear()
    {
        return $this->selectMax('articleYear', $this->_bindActive());
    }
    
    /**
     * check active articles
     * 
     * @return boolean
     */
    public function checkArticles()
    {
        return $this->selectTotal($this->_bindActive()) > 0;
    }
    
    /**
     * check article slug and status
     * 
     * @param string $slug
     * @return boolean
     */
    public function checkArticle($slug)
    {
        $where = $this->getHelper()->bindValue($slug, 'slug')
               . ' AND ' .  $this->_bindActive();
        return $this->selectTotal($where) > 0;
    }
    
    public function getAllArticles($limit = null)
    {
        $where = $this->_bindActive();
        return $this->select($where, 'articleYear DESC, id DESC', $limit);
    }

    public function getAllArticlesBySearch($limit = null,$keyword)
    {
        $where = $this->_bindActive();
        $table = new TableGateway(TEDWeb::COUNTRY);
        $countries = $table->selectPairs(['id','country']);
        $countries = array_map('strtolower', $countries);
        $input = preg_quote(strtolower($keyword), '~'); // don't forget to quote input string!
        $result = preg_filter('~' . $input . '~','$0', $countries);
        $where .= ' AND (`title` LIKE \'%'.$keyword.'%\' OR `subtitle` LIKE \'%'.$keyword.'%\' OR `city` LIKE \'%'.$keyword.'%\' OR `description` LIKE \'%'.$keyword.'%\' OR `architect` LIKE \'%'.$keyword.'%\' OR `website` LIKE \'%'.$keyword.'%\' ';
        if(count($result) > 0) {
            $where .= ' OR countryId IN(';
            foreach ($result as $key => $value) {
                $where .=  $key.',';
            }
            $where = rtrim($where,',');
            $where .= ')';
        }
        $where .= ' )';
        return $this->select($where, 'articleYear DESC, id DESC', $limit);
    }
    
    public function getArticleTotal()
    {
        $where = $this->_bindActive();
        return $this->selectTotal($where);
    }

    public function getArticleSearchTotal($keyword)
    {
        //$this->getSql()->join(TEDWeb::COUNTRY , 'country.id=winning_news.countryId',false);
        //$this->getSql()->join(['c' => TEDWeb::COUNTRY], 'c.id = winning_news.countryId', false);
        $where = $this->_bindActive();
        $a = 1;
        $table = new TableGateway(TEDWeb::COUNTRY);
        $countries = $table->selectPairs(['id','country']);
        $countries = array_map('strtolower', $countries);
        $input = preg_quote(strtolower($keyword), '~'); // don't forget to quote input string!
        $result = preg_filter('~' . $input . '~','$0', $countries);
        $where .= ' AND (`title` LIKE \'%'.$keyword.'%\' OR `subtitle` LIKE \'%'.$keyword.'%\' OR `city` LIKE \'%'.$keyword.'%\' OR `description` LIKE \'%'.$keyword.'%\' OR `architect` LIKE \'%'.$keyword.'%\' OR `website` LIKE \'%'.$keyword.'%\' ';
        if(count($result) > 0) {
            $where .= ' OR countryId IN(';
            foreach ($result as $key => $value) {
                $where .=  $key.',';
            }
            $where = rtrim($where,',');
            $where .= ')';
        }
        $where .= ' )';
        //echo $where;exit;
        return $this->selectTotal($where);
    }
    
    
    /**
     * bind active status
     * 
     * @return string
     */
    private function _bindActive()
    {
        return $this->getHelper()->bindOne('active');
    }
    
}
