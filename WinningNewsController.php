<?php
/**
 * Application WinningNews Controller
 * 
 * @category Application
 * @package  Application\Controller
 */

namespace Application\Controller;

use Application\ApplicationController;
use Application\Constant\Tab;
use Application\Form\WinningNewsSearchForm;
use Application\Model\WinningNewsModel;
use Application\Constant\WinningNewsConstant;
use Application\Utility\ArticleSlug;
use Web\View\Support\LookupStore;
use Web\Core\Plugin; 


class WinningNewsController extends ApplicationController
{
    private $_model;
    private $_yearNavigation;

    public function __construct()
    {
        parent::__construct();
        $this->_model = new WinningNewsModel();
        $this->_yearNavigation = false;
    }
    
    /**
     * index action
     * 
     * @return \Web\View\ViewModel
     */
    public function indexAction()
    {

        //redirect if there are no active articles
        if (! $this->_model->checkArticles()) {
            //$this->redirectBase();
        }
        $slug = new ArticleSlug();

        $form = new WinningNewsSearchForm();
        if($form->isPost()) {
            if($form->isValid()) {
                $keyword = $form->getValue('keywordSearch');
                if ($this->_yearNavigation) {
                    $year = $slug->year($this->getSlug());
                    if (false !== $year) {
                        if (! $this->_model->checkArticleYear($year)) {
                            $year = false;
                        }
                    }
                    if (false === $year) {
                        $year = $this->_model->getDefaultYear();
                    }
                    $this->view->setVariable('articles', $this->_model->getArticlesBySearch($year,$keyword));
                    $this->view->setVariable('activeYears', $this->_model->getYears());
                    $this->view->setVariable('selectedYear', $year);
                    $this->setHeadTitle('Winning News - ' . $year);
                } else {
                    $total = $this->_model->getArticleSearchTotal($keyword);
                    $page = $slug->page($this->getSlug());
                    $limit = 30;
                    $pages = ceil($total / $limit);
                    if ($page < 1 || $page > $pages) {
                        $page = 1;
                    }
                    $articlePaging = [
                        'total' => $total,
                        'page' => $page,
                        'pages' => $pages,
                        'limit' => $limit,
                    ];
                    $this->view->setVariable('articlePaging', $articlePaging);
                    $offset = ($page - 1) * $limit;
                    if (0 == $offset) {
                        $sqlLimit = $limit;
                    } else {
                        $sqlLimit = $offset . ',' . $limit;
                    }

                    $this->view->setVariable('articles', $this->_model->getAllArticlesBySearch($sqlLimit,$keyword));
                    $this->setHeadTitle('Winning News');
                }
            } else {
                $this->_getAllArticles();
            }

        } else {
            $this->_getAllArticles();

        }

        $this->view->setVariable('yearNavigation', $this->_yearNavigation);
        LookupStore::register('countries', $this->_model->getCountryOptions());

        $this->currentTab(Tab::WINNING_NEWS);
        $this->currentSubTab(Tab::SUBNAV_WINNING_NEWS);
        $this->registerForm($form);
        
        
        return $this->getViewModel();
    }
    
    /**
     * article action
     * 
     * @return \Web\View\ViewModel
     */
    public function articleAction() 
    {
        //redirect if there are no active articles
        if (! $this->_model->checkArticles()) {
            $this->redirectBase();
        }
        
        $slug = new ArticleSlug();
        $title = $slug->title($this->getSlug(false));
        $article = false;
        if (false !== $title) {
            if ($this->_model->checkArticle($title)) {
                $article = $this->_model->selectByColumn('slug', $title);
            }
        }
        if (false === $article) {
            $this->redirect('index/error');
        }

        $this->view->setVariable('images', $this->_model->getArticleImages($article->id));
        $this->view->setVariable('article', $article);
        $this->view->setVariable('slugs', $this->_getArticleNavigationSlugs($article));
        $country = $this->_model->getCountryById($article->countryId);
        $this->view->setVariable('country', $country);
            
        $this->plugin(Plugin::SCRIPT)->addModuleBody('winning-news-slider');
        $this->plugin(Plugin::SCRIPT)->addModuleBody('lightbox');
        $this->plugin(Plugin::STYLE)->addModuleStylesheet('lightbox');
        
        $this->currentTab(Tab::WINNING_NEWS);
        $this->currentSubTab(Tab::SUBNAV_WINNING_NEWS_ARTICLE);
        $metaTitle = 'Winning News Article : ' 
                   . $this->view->articleTitle($article->title, $article->city, $country) 
                   . ' - ' . $article->articleYear;
        $this->setHeadTitle($metaTitle);
        
        return $this->getViewModel();
    }
    
    
    /**
     * get slugs of next and previous articles
     * 
     * @param object $article
     * @return array
     */
    private function _getArticleNavigationSlugs($article)
    {
        if ($this->_yearNavigation) {
            $nextWhere = $this->_model->getSql()
                                       ->where('active', WinningNewsConstant::ACTIVE)
                                       ->where('articleYear', $article->articleYear)
                                       ->whereLess('id', $article->id);
            $nextArticleSlug = $this->_model->getArticleNavigationSlug($nextWhere, 'id DESC');
            $previousWhere = $this->_model->getSql()
                                           ->where('active', WinningNewsConstant::ACTIVE)
                                           ->where('articleYear', $article->articleYear)
                                           ->whereGreater('id', $article->id);
            $previousArticleSlug = $this->_model->getArticleNavigationSlug($previousWhere, 'id');
        } else {
            $nextWhere = $this->_model->getSql()
                                       ->where('active', WinningNewsConstant::ACTIVE)
                                       ->whereLess('articleYear', $article->articleYear, true)
                                       ->whereLess('id', $article->id);
            $nextArticleSlug = $this->_model->getArticleNavigationSlug($nextWhere, 'articleYear DESC, id DESC');
            $previousWhere = $this->_model->getSql()
                                           ->where('active', WinningNewsConstant::ACTIVE)
                                           ->whereGreater('articleYear', $article->articleYear, true)
                                           ->whereGreater('id', $article->id);
            $previousArticleSlug = $this->_model->getArticleNavigationSlug($previousWhere, 'articleYear, id');
        }
        return ['next' => $nextArticleSlug, 'previous' => $previousArticleSlug];
    }

    private function _getAllArticles()
    {
        $slug = new ArticleSlug();
        if ($this->_yearNavigation) {
            $year = $slug->year($this->getSlug());
            if (false !== $year) {
                if (! $this->_model->checkArticleYear($year)) {
                    $year = false;
                }
            }
            if (false === $year) {
                $year = $this->_model->getDefaultYear();
            }
            $this->view->setVariable('articles', $this->_model->getArticles($year));
            $this->view->setVariable('activeYears', $this->_model->getYears());
            $this->view->setVariable('selectedYear', $year);
            $this->setHeadTitle('Winning News - ' . $year);
        } else {
            $total = $this->_model->getArticleTotal();
            $page = $slug->page($this->getSlug());
            $limit = 30;
            $pages = ceil($total / $limit);
            if ($page < 1 || $page > $pages) {
                $page = 1;
            }
            $articlePaging = [
                'total' => $total,
                'page' => $page,
                'pages' => $pages,
                'limit' => $limit,
            ];
            $this->view->setVariable('articlePaging', $articlePaging);
            $offset = ($page - 1) * $limit;
            if (0 == $offset) {
                $sqlLimit = $limit;
            } else {
                $sqlLimit = $offset . ',' . $limit;
            }

            $this->view->setVariable('articles', $this->_model->getAllArticles($sqlLimit));
            $this->setHeadTitle('Winning News');
        }

    }
    
}
